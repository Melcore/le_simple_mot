let alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'à', 'â', 'æ', 'ç', 'é', 'è', 'ê', 'ë', 'î', 'ï', 'ô', 'œ', 'ù', 'û', 'ü', 'ÿ']


function setCaret(NodeOffset) {
    let range = document.createRange();
    let sel = window.getSelection();
    let nodus = NodeOffset.nodus
    let offset = NodeOffset.offset

    if(nodus.nodeType.nodeName === '#text'){
    	range.setStart(nodus, offset);
    }else{
    	range.setStart(nodus, offset);
    }
    range.collapse(true);
    sel.removeAllRanges();
    sel.addRange(range);
}

function getCaretIndex() {
  let element = document.getElementById("text_input")
  let position = 0;
  const isSupported = typeof window.getSelection !== "undefined";
  if (isSupported) {
    const selection = window.getSelection();
    if (selection.rangeCount !== 0) {
      const range = window.getSelection().getRangeAt(0);
      const preCaretRange = range.cloneRange();
      preCaretRange.selectNodeContents(element);
      preCaretRange.setEnd(range.endContainer, range.endOffset);
      position = preCaretRange.toString().length;
    }
  }
  return(position);
}

function indexToNode(ctrl, index){
    let offset = index
    let N_child = ctrl.childNodes.length
    let i = 0
    while(i < N_child){
        if (ctrl.childNodes[i].nodeName === '#text') {
            if (offset > ctrl.childNodes[i].textContent.length) {
                offset -= ctrl.childNodes[i].textContent.length
            } else {
                return {"offset": offset, "nodus" : ctrl.childNodes[i]}
            }
        }else{
            if (ctrl.childNodes[i].childNodes.length > 0)
                if (offset > ctrl.childNodes[i].childNodes[0].textContent.length) {
                    offset -= ctrl.childNodes[i].childNodes[0].textContent.length
                } else {
                    return {"offset": offset, "nodus": ctrl.childNodes[i].childNodes[0]}
                }
            else{
                return {"offset": 0, "nodus": ctrl.childNodes[i]}
            }
        }
        i++
    }
}



document.getElementById("text_input").addEventListener("input", function(event){
    let caractere
    let text_recreated = ""
    let caretIndex = getCaretIndex()

    for(let i = 0; i < event.target.textContent.length; i++){
        caractere = event.target.textContent[i]
        if(alphabet.indexOf(caractere.toLowerCase()) !== -1){
            let word = ""
            let j = 0
            let letter = event.target.textContent[i]
            while(i+j < event.target.textContent.length && alphabet.indexOf(letter.toLowerCase()) !== -1){
                word += letter
                j++
                letter = event.target.textContent[i+j]
            }

            if(available_words.indexOf(word.toLowerCase()) !== -1){
                text_recreated += "<span style='color:green'>"+word+"</span>"
            }else{
                text_recreated += "<span style='color:red'>"+word+"</span>"
            }
            i += j-1
        }else{
            text_recreated += caractere
        }
    }


    text_recreated += "<span></span>" // FIX : Bug d'espace en fin de ligne
    event.target.innerHTML = text_recreated
    let NodeOffset = indexToNode(event.target, caretIndex)
    setCaret(NodeOffset)
    event.target.focus()
})
